package com.remote;

import java.io.IOException;
import java.time.Duration;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.*;

import static java.lang.System.exit;

public class Engine {

    private int remoteCode;
    private String apiUrl;
    private Network network;
    private Boolean criticalError = false;

    public Engine (String apiUrl) throws Exception {
        System.out.print("Engine initialization...");
        this.apiUrl = apiUrl;
        this.generateRemoteCode();
        this.network = new Network(apiUrl);
        this.register();
        WindowManager.machineCode(this.getRemoteCode());
    }

    private void waitForChanges () {
        Timer timer = new Timer();
        TimerTask scheduler = new TimerTask() {
            @Override
            public void run() {
                try {
                    System.out.print("Waiting for instructions...");
                    fetchInstructions();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        timer.schedule(scheduler, 2000, 2000);
    }

    private void register () throws Exception {
        String param = "machine=" + this.remoteCode;
        String response = this.network.sendPost("register.php", param);
        System.out.print(response);
        if (!Objects.equals(response, "registered")) {
            this.criticalError = true;
            WindowManager.criticalError();
            this.exit(-1, 10000);
        } else {
            waitForChanges();
        }
    }

    private void exit (int code, int timeLag) {
        try {
            Thread.sleep(timeLag);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(code);
    }

    private void fetchInstructions () throws Exception {
        String param = "machine=" + this.remoteCode;
        String response = this.network.sendPost("instruction.php", param);
        if (!Objects.equals(response, "")) {
            System.out.print("Instruction found!");
            executeInstruction(response);
        }
    }

    private void executeInstruction(String instruction) throws IOException {
        Runtime runtime = Runtime.getRuntime();
        //System.out.print(instruction);
        Process proc = runtime.exec(instruction);
        if (Objects.equals(instruction, "shutdown -s -t 0")) {
            System.exit(0);
        }
    }

    private void generateRemoteCode () {
        this.remoteCode = ThreadLocalRandom.current().nextInt(0, 1000);
    }

    public int getRemoteCode () {
        return this.remoteCode;
    }

}
