package com.remote;


import javax.swing.*;
import java.awt.*;
import java.util.concurrent.Callable;

public class WindowManager {

    public static void createWindow (String message) {

    }

    public static void createWindowWithCallback (String message, String callbackName, Callable<Void> callbackFunc) {

    }

    public static void machineCode (int code) {
        String msg = "Your machine's code is " + code + ". \n Remote access: http://remotev2.esy.es";
        createFrame(msg, msg);
    }

    public static void criticalError () {
        createFrame("Critical error occurred", "Critical error has occurred. Application will shutdown in 10 seconds.");
    }

    private  static void createFrame (String title, String message) {
        JFrame frame = new JFrame(title);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 100);
        frame.setLayout(new GridLayout(3, 1));
        frame.getContentPane()
                .add(new Label(message), BorderLayout.CENTER);
        frame.pack();

        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

}
