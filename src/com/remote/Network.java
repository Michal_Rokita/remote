package com.remote;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

// import javax.net.ssl.HttpsURLConnection;

class Network {

    private static final String USER_AGENT = "Mozilla/5.0";
    private String response;
    private String apiUrl;

    Network(String apiUrl) throws Exception {

        this.apiUrl = apiUrl;
    }

    String sendPost(String method, String urlParameters) throws Exception {

        // Establishing the connection

        URL obj = new URL(this.apiUrl + method);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

        // Request headers

        connection.setRequestMethod("POST");
        connection.setRequestProperty("User-Agent", USER_AGENT);
        connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        // Sending the request

        connection.setDoOutput(true);
        DataOutputStream stream = new DataOutputStream(connection.getOutputStream());
        stream.writeBytes(urlParameters);
        stream.flush();
        stream.close();

        // Getting the response

        int responseCode = connection.getResponseCode();

        BufferedReader input = new BufferedReader(
                new InputStreamReader(connection.getInputStream())
        );
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = input.readLine()) != null) {
            response.append(inputLine);
        }

        input.close();

        // System.out.print(response);

        this.response = response.toString();
        return response.toString();

    }

}
